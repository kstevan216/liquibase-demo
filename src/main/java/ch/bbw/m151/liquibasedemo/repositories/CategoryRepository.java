package ch.bbw.m151.liquibasedemo.repositories;

import ch.bbw.m151.liquibasedemo.datamodel.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, String> {
}
