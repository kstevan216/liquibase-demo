package ch.bbw.m151.liquibasedemo;

import ch.bbw.m151.liquibasedemo.repositories.ArticleRepository;
import ch.bbw.m151.liquibasedemo.repositories.CategoryRepository;
import ch.bbw.m151.liquibasedemo.repositories.CommentRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@SpringBootTest(properties = "spring.liquibase.drop-first=true")
class LiquibaseDemoApplicationTests implements WithAssertions {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void testIfDataIsLoadedIntoDatabase() {
        assertNotNull(articleRepository.findAll());
        assertNotNull(commentRepository.findAll());
        assertNotNull(categoryRepository.findAll());
    }
}

